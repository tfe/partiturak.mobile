import React from "react";
import { TouchableOpacity, Dimensions, FlatList, StyleSheet, Text, View, ScrollView, Button } from 'react-native';
import {useState, useEffect} from 'react';
import constants from './Constants.js';
import Card from './Card.js';
import AsyncStorage from '@react-native-async-storage/async-storage';
import globalStyles from './Styles.js';
import { MaterialIcons } from '@expo/vector-icons';



export default ({ route, navigation }) => {

    const [authors, setAuthors] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [portrait, setPortrait] = useState(true);

    const [firstAuthors, setFirstAuthors] = useState([]);
    const [firstTypes, setFirstTypes] = useState([]);

    const [types, setTypes] = useState([]);

    const setItems = function(fetched)
    {
        setFirstTypes(Object.values(fetched.popular_types).splice(0,5));
        setFirstAuthors(Object.values(fetched.popular_authors).splice(0,5));

        let tags = Object.values(fetched.tags);
        let tmp_types = Object.values(tags).filter((x) => x.isType).sort((x,y) => x.name>y.name ? 1 : -1);
        tmp_types.sort((a,b) => b.name.toLowerCase()<a.name.toLowerCase() ? 1 : -1);
        setTypes(tmp_types);

        let tmp_authors = Object.values(tags).filter((x) => x.isAuthor).sort((x,y) => x.name>y.name ? 1 : -1);
        tmp_authors.sort((a,b) => b.name.toLowerCase()<a.name.toLowerCase() ? 1 : -1);
        setAuthors(tmp_authors);
    }

    const refresh = async () => {

        let request = await fetch('https://partiturak.eus/generic_tags');
        if(request)
        {
            let fetched = await request.json();
            if(fetched)
            {
                await AsyncStorage.setItem('partiturak.eus/tags_fetched4', JSON.stringify(fetched));
                setItems(fetched);
            }
    }
    };

    useEffect((x) => {
        AsyncStorage.getItem('partiturak.eus/tags_fetched4').then((s) => 
            {
                if(s)
                {
                    setItems(JSON.parse(s));
                }
                refresh();
            });
    }, []);

    useEffect((x) => {
        if(isLoading)
        {
            setLoading(false);
            //setItems({ tags: tags});
        }
    }, [isLoading]);

    return (
        <View style={styles.container}>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>Partiturak.eus</Text>
            </View>
            <View style={styles.listContent}>
                <TouchableOpacity style={ styles.content}
                    onPress={() => {
                        navigation.push('Intro', { } );
                    }}>
                      <MaterialIcons
                      name='star'
                      style={{ marginRight: 2, paddingLeft:20,paddingRight:0 }}
                      color={ constants.web_color2 }
                      size={20}
                      />
                    <Text style={styles.tag}>Hasiera</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.content}
                    onPress={() => {
                        navigation.push('List', { tag: { id: null, type:'all', titleName:'Partiturak', name:'Partitura Guztiak'}} );
                    }}>
                      <MaterialIcons
                      name='star'
                      style={{ marginRight: 2, paddingLeft:20,paddingRight:0 }}
                      color={ constants.web_color2 }
                      size={20}
                      />
                    <Text style={styles.tag}>Partitura guztiak</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.titleContainer}>
                <Text style={styles.title}>Kategoriak</Text>
            </View>
            { firstTypes.length>0 &&
                    <ScrollView style={ styles.types }>
                        { firstTypes.map((item,index) => { return (
                        <View style={{ width: '100%' }} key={'idx'+index}>
                            <TouchableOpacity key={index} style={ styles.content } onPress={() => { navigation.push('List', { tag: item} ); }}>
                                  <MaterialIcons
                                  name='star'
                                  style={{ marginRight: 2, paddingLeft:20,paddingRight:0 }}
                                  color={ constants.web_color2 }
                                  size={20}
                                  />
                                <Text style={styles.tag}>{ item.name }</Text>
                            </TouchableOpacity>
                        </View>
                    )}) }
                    <View style={ styles.ruler} />
                        { types.map((item,index) => { return (
                        <View style={{ width: '100%' }} key={'idx'+index}>
                            <TouchableOpacity key={index} style={ styles.content } onPress={() => { navigation.push('List', { tag: item} ); }}>
                                <Text style={styles.tag}>{ item.name }</Text>
                            </TouchableOpacity>
                        </View>
                    )}) }
                </ScrollView>
            }

            <View style={styles.titleContainer}>
                <Text style={styles.title}>Autoreak</Text>
        </View>
            { firstAuthors.length>0 &&
                    <ScrollView style={ styles.authors }>
                        { firstAuthors.map((item,index) => { return (
                        <View style={{ width: '100%' }} key={'idx'+index}>
                            <TouchableOpacity key={index} style={ styles.content } onPress={() => { navigation.push('List', { tag: item} ); }}>
                                  <MaterialIcons
                                  name='star'
                                  style={{ marginRight: 2, paddingLeft:20,paddingRight:0 }}
                                  color={ constants.web_color2 }
                                  size={20}
                                  />
                                <Text style={styles.tag}>{ item.name }</Text>
                            </TouchableOpacity>
                        </View>
                    )}) }
                    <View style={ styles.ruler} />
                        { authors.map((item,index) => { return (
                        <View style={{ width: '100%' }} key={'idx'+index}>
                            <TouchableOpacity key={index} style={ styles.content } onPress={() => { navigation.push('List', { tag: item} ); }}>
                                <Text style={styles.tag}>{ item.name }</Text>
                            </TouchableOpacity>
                        </View>
                    )}) }
                </ScrollView>
            }





        </View>
    );
};

const styles = StyleSheet.create({
    container:
    {
        flex:1,
        flexDirection:'column',
        height:'100%'
    },
    content: {
        width: '100%',
        backgroundColor: constants.web_color4,
        textColor:'#fff',
        alignItems: 'center',
        flexDirection:'row',
        justifyContent: 'flex-start',
    },
    listContent: {
        width:'100%',
        paddingBottom: 10,
    },
    list: {
        flex:1,
        width: '100%',
        flexDirection: 'row',
        flexWrap:'wrap',
        backgroundColor: constants.lightBackground,
        textColor:'#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleContainer: {
        paddingTop:15,
        paddingLeft:15,
        paddingBottom:15,
        backgroundColor: constants.web_color2,
    },
    title: {
        color:constants.web_color6,
    },
    ruler: { borderBottomColor: 'black', borderBottomWidth: StyleSheet.hairlineWidth, },
    tag: {
        paddingTop:10,
        paddingLeft:15,
        paddingBottom:10,
        backgroundColor: constants.lightBackground,
    },
});
