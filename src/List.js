import React from "react";
import { ActivityIndicator, Dimensions, FlatList, RefreshControl, StyleSheet, Text, View, Button } from 'react-native';
import {useState, useEffect, useRef} from 'react';
import constants from './Constants.js';
import Card from './Card.js';
import AsyncStorage from '@react-native-async-storage/async-storage';
import globalStyles from './Styles.js';
import ListRightMenu from './ListRightMenu.js';
import { FontAwesome } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { TextInput } from 'react-native';


const numLinesDisplayed=30;



export default ({ route, navigation}) => {

    const [open, setOpen] = React.useState(false);
    const [filteredList, setFilteredList] = useState([]);
    const [columns, setColumns] = useState(1);
    const [portrait, setPortrait] = useState(true);
    const [tag, setTag] = useState(null);
    const [page, setPage] = useState(1);
    const [isLoading, setIsLoading] = useState(true);

    const [realSearch, setRealSearch] = useState('');
    const [realSortType, setRealSortType] = useState(0);

    const [idsIndex, setIdsIndex] = useState([]);
    const [orderedList, setOrderedList] = useState([]);
    const [tags, setTags] = useState({});


    useEffect((x) => {
        AsyncStorage.getItem('partiturak.eus').then((s) => 
            {
                if(s)
                {
                    setItems(JSON.parse(s));
                }
                refresh();
            });
        const sub = Dimensions.addEventListener('change', updateColumns);

        return () => {
            console.log('remove sub list');
            sub?.remove();
        };
    }, []);


    const setItems = function(fetched)
    {
        let all = Object.values(fetched.musicsheets || []);
        let ids = {};
        all.forEach((x, idx) => {
            ids[x.id] = idx;
        });
        setIdsIndex(ids);
        setTags(fetched.tags);
        setOrderedList(all);
    }


    const refresh = async () => {

        setIsLoading(true);
        let request = await fetch('https://partiturak.eus/generic_data');
        if(request)
        {
            let fetched = await request.json();
            if(fetched)
            {
                await AsyncStorage.setItem('partiturak.eus', JSON.stringify(fetched));
                setItems(fetched);
                setIsLoading(false);
            }
        }
    };

    useEffect((x) => {
        navigation.setOptions({
              headerTitle: tag && tag.name ? (tag.titleName ? tag.titleName : tag.name) : 'Partiturak',
              headerRight: () => (<ListRightMenu refresh={refresh} setRealSortType={setRealSortType}  setIsLoading={setIsLoading} setRealSearch={setRealSearch} realSortType={realSortType} />),
        })
    }, [tag, realSortType]);

    const applyFilters = function(all)
    {
        if(isLoading)
        {
            return setFilteredList([]);
        }
        console.log('showing filters');

        if(tag && tag.id!==null)
        {
            all = all.filter((x) => x.tags.indexOf(tag.id)!==-1);
        }
        if(realSearch)
        {
            let s = realSearch.toLowerCase();
            all = all.filter((x) => (x.name.toLowerCase().indexOf(s)!==-1 || (x.text && x.text.toLowerCase().indexOf(s)!==-1)));
        }
        if(realSortType===0)
        {
            all.sort((a,b) => {
                return b.id - a.id
            });
        }
        else if(realSortType===1)
        {
            all.sort((a,b) => {
                return a.name > b.name ? 1 : -1
            });
        }
        else
        {
            all.sort((a,b) => {
                return b.views - a.views
            });
        }

        let newItems = [...all].splice(0, numLinesDisplayed*(page+1)*columns);
        setFilteredList(newItems);
        setPage(page+1);
    }

    const updateColumns = () => {
        let windowWidth = Dimensions.get('window').width;
        let div = Math.floor(windowWidth/300);

        if(columns!=div && div>0)
        {
            setColumns(div);
        }
    };

    const forcedTag=null;
    useEffect((x) => {
        setTag({id:forcedTag});
    }, [forcedTag]);

    useEffect((x) => {
        if(route?.params?.tag)
        {
            setTag(route.params.tag);
        }
        if(route?.params?.sort!==undefined)
        {
            setRealSortType(route.params.sort);
        }
    }, [route,navigation]);

    useEffect((x) => {
        updateColumns();
        applyFilters(orderedList);
    }, [ isLoading, realSortType, realSearch, orderedList, tag ]);


    return (
        <View style={ styles.content }>

            <View style={ styles.content }>

                <View style={ styles.list }>

                    <FlatList data={filteredList}
                    key={columns}
                    numColumns={columns}
                    onEndReachedThreshold={0.5}

                    onStartReached={ (x) => {
                    }}
                    onEndReached={ (x) => {
                        applyFilters(orderedList);
                    }}
                    refreshControl={
                        <RefreshControl
                        refreshing={isLoading}
                        onRefresh={refresh}
                        />
                    }

                    renderItem={({item, index, separators}) => (
                        <View style={{ width: (100/columns)+'%' }} key={'card'+item.id}>
                            <Card key={item.id } props={{ tags: tags, route: route, navigation: navigation, index:index, item: item}} />
                        </View>
                    )}>
                    </FlatList>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    content: {
        flex:1,
    },
    message: {
        height:'100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    list:
    {
        flex:1,
        backgroundColor: constants.web_color4,
        padding:5,
        gap:0
    },
});



