import React from "react";
import { View, Text, StyleSheet, ActivityIndicator} from 'react-native';


export default ({ route , navigation, props }) => {

    return (
                <View style={ styles.content }>
                    <View style={ styles.message }>
                        <Text>Kargatzen...</Text>
                        <ActivityIndicator size="large" color="#00ff00" />
                    </View>
                </View>
    );
};

const styles = StyleSheet.create({
    content: {
        flex:1,
    },
    message: {
        height:'100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    list:
    {
        flex:1
    },
});
