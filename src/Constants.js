export default {
    darkBackground: '#222',
    lightBackground:'#fff',
    borderColor: '#555',
    darkText: '#333',

    web_localesbg: "#313844",
    web_localesfg: "#f0f0f0",
    web_color2: "#f0f0f0",
    web_color3: "#313844",
    web_color4: "#ffffff",
    web_color5: "#9f6d41",
    web_green: "#399547",
    web_color6: "#88613f",
    web_color7: "#e3e6ec",
    web_warning: "#c06060",
};
