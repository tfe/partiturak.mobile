import { StyleSheet } from 'react-native';
import constants from './Constants.js';

export default StyleSheet.create({
    title: {
        fontSize:15,
        textAlign:'center',
        backgroundColor: constants.darkBackground,
        color: constants.lightText,
        paddingTop:15,
        paddingBottom:15,
    },
    buttonContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 0,
        paddingHorizontal: 0,
        borderRadius: 10,
        elevation: 3,
        backgroundColor: constants.web_color3,
    },
    button: {
        paddingTop:25,
        paddingBottom:25,
        backgroundColor: constants.web_color3,
        color: constants.web_color2
    },
});

