import React from "react";
import { Dimensions, Image, TouchableOpacity, StyleSheet, Text, View, Button } from 'react-native';
import constants from './Constants.js';
import {useState, useEffect} from 'react';

import ScaledImage from './ScaledImage.js';
import { AntDesign } from '@expo/vector-icons';
import * as FileSystem from 'expo-file-system';
import Pdf from 'react-native-pdf';


const windowDimensions = Dimensions.get('window');
const screenDimensions = Dimensions.get('screen');


export default ({ navigation, uri }) => {

    const [dimensions, setDimensions] = useState({
        window: windowDimensions,
        screen: screenDimensions,
    });
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;

    useEffect(() => {
        const subscription = Dimensions.addEventListener(
            'change',
            ({window, screen}) => {
                setDimensions({window, screen});
            },
        );
        return () => subscription?.remove();
    });

    return (
    <View style={ styles.content }>
            <Pdf
                    trustAllCerts={false}
                    source={{uri: uri}}
                    fitWidth={true}
                    maxScale= {30}
                    minScale={0.4}
                    horizontal={true}
                    onLoadComplete={(numberOfPages,filePath) => {
                    }}
                    onPageChanged={(page,numberOfPages) => {
                    }}
                    onError={(error) => {
                    }}
                    onPressLink={(uri) => {
                    }}
                    style={{
                        flex:1,
                        backgroundColor:'#ffffff',
                        width: dimensions.window.width,
                        height:  dimensions.window.height
                    }}/>
    </View>);
};

const styles = StyleSheet.create({
    content: {
        flex: 1,
        width: '100%',
        flexDirection: 'column',
        height: '100%',
        backgroundColor: constants.lightBackground,
        textColor:'#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    title:
    {
        fontSize:35,
        marginTop:15,
        marginBottom:15,
    },
    image:
    {
        width:'100%',
        opacity:1,
    }
});
