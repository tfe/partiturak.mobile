import React from 'react';
import { TouchableOpacity, StyleSheet, Text, Linking } from 'react-native';
import { View } from 'react-native';
import constants from './Constants.js';
import { AntDesign } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { TextInput } from 'react-native';
import { useRef} from 'react';
import {useState, useEffect } from 'react';
import { Menu, MenuOptions, MenuOption, MenuTrigger, } from 'react-native-popup-menu';

const ListRightMenu = (props) => {
    const { url, children, style = {}  } = props;
    
    const [searchVisible, setSearchVisible] = useState(false);
    const [sortType, setSortType] = useState(0);
    const [search, setSearch] = useState('');
    const onPress = () => Linking.canOpenURL(url).then(() => {
        Linking.openURL(url);
    });

    const sorts = [
        { icon: 'access-time', key: 'createdAt' },
        { icon: 'sort-by-alpha', key: 'name' },
        { icon: 'remove-red-eye', key: 'views' }
    ];
    const inputEl = useRef();

    let searchTimer = React.useRef();
    useEffect((x) => {
        if(props?.realSortType!==undefined)
        {
            setSortType(props?.realSortType);
        }
    }, [ props ]);

    useEffect((x) => {
        clearTimeout(searchTimer.current);
        props.setIsLoading(true);
        searchTimer.current = window.setTimeout(function() {
            props.setRealSearch(search);
            props.setIsLoading(false);
        }, 500);
    }, [ search ]);

    let sortTimer = React.useRef();
    useEffect((x) => {
        clearTimeout(sortTimer.current);
        props.setIsLoading(true);
        sortTimer.current = window.setTimeout(function() {
            props.setRealSortType(sortType);
            props.setIsLoading(false);
        }, 500);
    }, [ sortType ]);


    return (
          <View style={{ justifyContent:"flex-end", paddingLeft:10, width:'80%', height:'100%', flexDirection:'row', alignItems:"center" }}>

              { !searchVisible && 

                          <Menu>
                              <MenuTrigger>
                                        <View style={{flexDirection:"column", paddingLeft:10, paddingRight:10, justifyContent:"center", alignItems:"center"}}>
                                          <MaterialIcons
                                          name={ sorts[sortType].icon }
                                          style={{ marginRight: 2  }}
                                          color={ constants.web_color2 }
                                          size={20}
                                          />
                                        <Text style={{color:"white", fontSize:10}}>Ordenatu</Text>
                                        </View>
                              </MenuTrigger>
                              <MenuOptions>
                                   <MenuOption key="0" text="Egunaren arabera (Berrienetik Zaharrenera)" onSelect={() => { setSortType(0); } }  />
                                   <MenuOption key="1" text="Izenaren arabera (A-tik Z-ra)" onSelect={() => { setSortType(1); } }  />
                                   <MenuOption key="2" text="Ikustaldiaren arabera (Ezagunenak aurretik)" onSelect={() => { setSortType(2); } }  />
                              </MenuOptions>
                          </Menu>

              }

              { searchVisible && (
                  <TextInput
                  ref={inputEl}
                  autoFocus={true}
                  style={ {
                          padding:6,
                          backgroundColor: constants.lightBackground,
                          color:constants.web_color3,
                          width: '100%',
                          marginRight:0
                  } }
                  placeholderTextColor="#aaa"
                  placeholder="Bilaketa"
                  onSubmitEditing={(e) => { } }
                  onChangeText={(e) => {setSearch(e); }  }
                  value={ search }
                  /> ) }

              { searchVisible && (
                  <AntDesign
                  name='closecircle'
                  style={{ marginRight: 2, padding:15 }}
                  color={ constants.web_color2 }
                  onPress={() => { setSearch(''); setSearchVisible(false);  }}
                  size={20}
                  />
              ) }

              { !searchVisible && (
                    <TouchableOpacity
                      onPress={() => { setSearchVisible(true);  }}>
                        <View style={{flexDirection:"column", paddingRight:15, paddingLeft:15, justifyContent:"center", alignItems:"center"}}>
                          <AntDesign
                          name='search1'
                          style={{ marginRight: 5, padding:0 }}
                          color={ constants.web_color2 }
                          size={20}
                          />
                          <Text style={{color:"white", fontSize:10}}>Bilatu</Text>
                        </View>
                    </TouchableOpacity>
              )}
          </View>
    );
};

const styles = StyleSheet.create({
    text: {
        fontSize: 16
    },
});

export default ListRightMenu;
