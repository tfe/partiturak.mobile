import React from 'react';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import globalStyles from './Styles.js';

const ShortcutList = (props) => {
    const { url, children, style = {}  } = props;
    

    return (
        <TouchableOpacity onPress={() => { props.navigation.push(props.dest,  props.conf ); }}>
            <View style={globalStyles.buttonContainer}>
                <Text style={globalStyles.button}>{ props.title}</Text>
            </View>
        </TouchableOpacity>
    );
};


export default ShortcutList;
